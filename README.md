# Docker常用命令及说明

## 基础命令

```bash
# 查看Docker版本信息
docker version

# 查看系统信息
docker info

# 查看帮助
docker
docker --help
docker run --help
docker container --help

# 登录docker hub
docker login

# 退出docker hub
docker logout

# 登录harbor
docker login harbor.gfstack.geo

# 退出harbor
docker logout harbor.gfstack.geo
```

## 镜像相关

```bash
# 删除/opt目录下的文件
rm -rf /opt/*

# 拉取镜像
docker pull nginx
docker pull harbor.gfstack.geo/3rd/haproxy:1.9.7
docker pull harbor.gfstack.geo/geostack/jre:8u202

# 构建镜像（cd到Dockerfile所在目录，最后的一个点不能少）
docker build -t IMAGE[:TAG] .
docker build -t IMAGE[:TAG] IMAGE[:TAG] IMAGE[:TAG] .

# 构建镜像（指定Dockerfile文件路径，最后的一个点不能少）
docker build -f ~/docker-images/jre/Dockerfile -t jre:8u202 .

# 打标签（重命名镜像）
docker tag IMAGE[:TAG] IMAGE[:TAG]

# 推送镜像（推送镜像前必须先登录）
docker push harbor.gfstack.geo/3rd/haproxy-test:1.9.7

# 将镜像导出到tar文件
docker save harbor.gfstack.geo/3rd/haproxy:1.9.7 > /opt/haproxy-image.tar
docker save -o /opt/image-all.tar harbor.gfstack.geo/3rd/haproxy:1.9.7 harbor.gfstack.geo/geostack/jre:8u202

# 删除镜像
docker rmi -f IMAGE

# 从tar文件中导入镜像
docker load < /opt/haproxy-image.tar
docker load -i /opt/image-all.tar

# 删除/opt目录下的文件
rm -rf /opt/*

# 列出镜像
docker images

# 列出镜像，不截断IMAGE ID（默认截断为12位）
docker images --no-trunc

# 列出镜像，包含中间镜像
docker images -a

# 查看镜像底层信息
docker inspect IMAGE
```

## 启动停止

```bash
# 以交互模式启动一个容器并分配一个伪终端
docker run -it harbor.gfstack.geo/geostack/jre:8u202

# 启动一个centos:7.6.1810的容器，退出容器时自动销毁
docker run -it --rm centos:7.6.1810

# 在后台启动一个centos:7.6.1810的容器，并打印容器ID
docker run -itd centos:7.6.1810

# 后台启动一个Tomcat并将容器的8080端口映射到宿主的8844端口（宿主端口在前，容器端口在后）
docker run -d -p 8844:8080 harbor.gfstack.geo/geostack/apache-tomcat:8.5.40

# 后台启动一个Tomcat并将容器暴露的所有端口映射到宿主的随机端口
docker run -d -P --name=tomcat --restart=always harbor.gfstack.geo/geostack/apache-tomcat:8.5.40

# 启动容器传递环境变量挂载文件（宿主文件在前，容器文件在后）
echo "Docker培训开始了" > /opt/info.txt
docker run -it -e WANGRUI_AGE=30 -e DINGHAO_AGE=30 -v /opt/info.txt:/srv/info.txt harbor.gfstack.geo/geostack/jre:8u202

# 重启容器
docker restart CONTAINER

# 停止容器
docker stop CONTAINER

# 强行杀死容器（慎用）
docker kill CONTAINER

# 删除一个容器
docker rm -f CONTAINER
```

## 调试相关

```bash
# 列出运行的容器
docker ps

# 列出所有的容器
docker ps -a

# 列出所有的容器，不截断CONTAINER ID（默认截断为12位）
docker ps -a --no-trunc

# 显示最后创建的一个容器
docker ps -l

# 显示最后创建的n个容器
docker ps --last n

# 查看容器底层信息
docker inspect CONTAINER

# 查看docker日志
docker logs CONTAINER

# 查看docker实时日志
docker logs -f CONTAINER

# 查看1小时以来的docker日志
docker logs -f --since 1h CONTAINER

# 查看10分钟以来的docker日志
docker logs -f --since 10m CONTAINER

# 查看60秒以来的docker日志
docker logs -f --since 60s CONTAINER

# 交互模式进入容器
docker exec -it CONTAINER COMMAND

# 退出容器
`ctrl` + D
`ctrl` + P + Q

# 从容器拷贝文件到宿主
docker cp CONTAINER:/srv/tomcat8/conf /opt/temp

# 从宿主拷贝文件到容器
docker cp /opt/temp2 CONTAINER:/opt/temp2

# 暂停一个或多个容器中的进程
docker pause CONTAINER

# 解除暂停进程
docker unpause CONTAINER

# 检查容器的文件系统中文件或目录的更改
docker diff CONTAINER
```

## 其他命令

```bash
# 重命名容器
docker rename CONTAINER NEW_NAME

# 容器资源使用情况统计
docker stats

# 查看容器内运行的进程
docker top CONTAINER

# 查看容器的端口映射
docker port CONTAINER

# 更新容器配置
docker update [OPTIONS] CONTAINER
```



## 参考连接

Docker run reference（Docker命令行参考）

https://docs.docker.com/engine/reference/run/

Dockerfile reference（Dockerfile参考）

https://docs.docker.com/engine/reference/builder/

Compose file version 3 reference（Docker Compose参考）

https://docs.docker.com/compose/compose-file/


Portainer（Docker WebUI）

https://github.com/portainer/portainer

https://hub.docker.com/r/portainer/portainer

